# Mood of the Month
=====
        TODO:Sommaire 

Le but de ce projet est de créer un site qui pourra récolter le mood des utilisateurs.

Afin de vous connecter, merci d'effectuer les tâches suivantes en amont. Nous vous proposons également des étapes
pour tester notre application pas à pas.

1. Exécuter la DB mysql. Si vous avez docker, vous pouvez utiliser la commande suivante:
```
docker run --name mariadb --rm -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_DATABASE=defaultdb -p 3306:3306 -v "%cd%/initdb:/docker-entrypoint-initdb.d" mariadb
```
2. Dans le dossier "initdb", vérifiez que les tables, ainsi que les entrées par défaut sont bien créées.

3. Run l'application, et se connecter à localhost:8080/login. Vous serez redirigé vers une page de connexion.
Pour l'occasion, nous vous avons créé un identifiant, un mot de passe, et même une adresse mail, qui vous sera utile plus tard !

    User : admin
    Pwd : USRTniUn
    adresse gmail : testjava.epf@gmail.com  Mot de passe :  TestJavaEPF

         Une fois connecté, vous avez accès au dashboard (localhost:8080/admin/dashboard) 

4. Essayez de créer un utilisateur, en remplissant chacun des champs. Attention : Veillez à bien rentrer une 
adresse mail valide et à respecter le format des dates !
Format des dates : 1996-05-21

NB: Lorsqu'un utilisateur est crée, un mail lui est envoyé automatiquement pour lui donner son password encrypté. 
Il peut à partir de ce password, se connecter. Mais dans notre application, nous n'avons pas jugé nécessaire que l'utilisateur
se connecte car il n'a rien à voir. Lorqu'il essaiera de se connecter, une erreursera renvoyée, car la page de succès par défaut
est le dashboard, qui ne peut etre accessible que par l'administrateur.
Pour se connecter, l'utilisateur entre son adresse mail et son password.

5. L'envoi des mails est censé être automatique aux utilisateurs, pour envoyer un mail manuellement : 
            
    http://localhost:8080/sendHtmlEmail/"AdresseMail_du_User_créé"

6. Par mail, un lien vers le questionnaire a été envoyé à l'utilisateur ! Il peut, au moyen de ce lien, répondre au questionnaire d'humeur. (localhost:8080/motm_survey/idutilisateurcrée)

7. Retournez sur le dashboard administrateur, et rafraichissez la page, l'humeur de l'utilisateur a bien été saisie.


Liste des fonctionnalités :
    1. Connexion par un administrateur et accès à son dashboard.
    2. Création, modification, suppression d'un User par l'Administrateur.
    3. Envoi de mail de rappel aux utilisateurs avec un lien vers leur questionnaire de réponse
    4. Actualisation automatique du dashboard, et affichage des statistiques

