package io.takima.demo.Utils;


import io.takima.demo.Controller.AdminController;
import io.takima.demo.Modele.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Random;

public class Password {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    AdminController adminController;
    //encrypt password with bcryptpasswordencoder
    public static String encryptePassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    /**
     * methode prenant en parametre une personne pour ensuite generer son pwd
     * @param u
     * @param role
     * @return
     */
    public String generate(User u, String role){
        Random rand = new Random();
        String nom=u.getLastName().substring(0,1);
        String prenom=u.getFirstName().substring(0,1);
        String r= role.substring(0,2).toUpperCase();
        StringBuilder password = new StringBuilder(r + nom + prenom);
        for (int i=0 ;  i <4;i++){
            char c =(char) (rand.nextInt(32)+80);
            password.append(c);
        }
        System.out.println(password.toString());


        //String result = new SimpleEmailController().sendSimpleEmail(p, password.toString());
        System.out.println("employe2");
        //System.out.println("Message : " + result);
        return password.toString();

    }

//    public String generateEncrypt(String password){
//        return encryptePassword(password);
//    }




}
