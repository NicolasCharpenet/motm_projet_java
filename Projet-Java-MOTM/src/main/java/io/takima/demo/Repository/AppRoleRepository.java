package io.takima.demo.Repository;

import io.takima.demo.Role.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppRoleRepository extends JpaRepository<AppRole, Long> {

    public AppRole findByRoleName(String roleName);

}
