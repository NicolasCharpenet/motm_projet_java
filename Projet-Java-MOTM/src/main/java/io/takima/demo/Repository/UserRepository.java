package io.takima.demo.Repository;

import io.takima.demo.Modele.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    public User findByEmail (String email);
    public User findByMotm (int motm);

}
