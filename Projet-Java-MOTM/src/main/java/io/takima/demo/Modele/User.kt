package io.takima.demo.Modele

import io.takima.demo.Role.AppUser
import org.springframework.data.jpa.repository.Query
import java.sql.Date
import javax.persistence.*
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;



/**
 *
 */

@Entity(name = "users")
@NamedQuery(name = "CalculateAvgMotm", query = "select avg(motm) from users") // Requete JPSQL vouée à calculer la moyenne
                                                                            // du mood du mois des utilisateurs

data class User(
            @GeneratedValue(strategy = GenerationType.IDENTITY)
            @Id var id: Long?,
            @Column(name = "first_name") var firstName: String?,
            @Column(name = "last_name") var lastName: String?,
            @Column(name = "age") var age: Int?,
            @Column(name = "datenaiss") var datenaiss: Date?,
            @Column(name = "email") var email: String?,
            @Column(name = "motm") var motm: Int?)

    {
        constructor() : this(null, null, null, null, null, null, null)
    }


    @OneToOne(mappedBy = "user")
    private val appUser: AppUser? = null;


