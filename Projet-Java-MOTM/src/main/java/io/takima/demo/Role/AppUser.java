package io.takima.demo.Role;


import io.takima.demo.Modele.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
//@NoArgsConstructor
//@AllArgsConstructor
@Table(name = "App_User",
        uniqueConstraints = @UniqueConstraint(name = "APP_USER_UK", columnNames = "User_Name"))
public class AppUser {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(insertable = false, updatable = false)
    private Long userid;


    @Column(name = "User_Name", length = 36, nullable = false)
    private String userName;

    @Column(name = "Encrypted_Password", length = 128, nullable = false)
    private String encryptedPassword;

    @Column(name = "enabled", length = 1, nullable = false)
    private boolean enabled;

    private String role;

    @OneToOne
    private User user;


    public AppUser() {
    }

    public AppUser(Long userId, String userName, String encryptedPassword, boolean enabled, String role, User user) {
        this.userid = userId;
        this.userName = userName;
        this.encryptedPassword = encryptedPassword;
        this.enabled = enabled;
        this.role = role;
        this.user = user;
    }

    public AppUser(String mail, String encryptedPassword, boolean b, String role, User user) {
        this.userName = mail;
        this.encryptedPassword = encryptedPassword;
        this.enabled = b;
        this.role = role;
        this.user = user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
