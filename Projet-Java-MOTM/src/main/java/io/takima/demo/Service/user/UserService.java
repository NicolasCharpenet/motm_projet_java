package io.takima.demo.Service.user;

import io.takima.demo.Modele.User;

public interface UserService {
    public User findByEmail (String email);
    public User findByMotm(int motm);
}
