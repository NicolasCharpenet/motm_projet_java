package io.takima.demo.Service.appUser;


import io.takima.demo.Repository.AppUserRepository;
import io.takima.demo.Role.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

@Service
public class AppUserServiceImpl implements AppUserService{

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private AppUserRepository appUserRepository;

    public AppUser findUserAccount(String userName) {
        try {
            String sql = "Select user from "+AppUser.class.getName()+" user " //
                    + " Where user.userName = :userName ";

            Query query = entityManager.createQuery(sql, AppUser.class);
            query.setParameter("userName", userName);

            return (AppUser) query.getSingleResult();
        } catch (NoResultException user) {
            return null;
        }
    }

    @Override
    public AppUser enregistrerAppuser(AppUser appUser) {
        return appUserRepository.save(appUser);
    }
}
