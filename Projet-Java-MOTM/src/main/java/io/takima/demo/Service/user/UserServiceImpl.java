package io.takima.demo.Service.user;

import io.takima.demo.Modele.User;
import io.takima.demo.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired //injection de l'interface Repository
    private UserRepository userRepository;


    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User findByMotm(int motm) {
        return userRepository.findByMotm(motm);
    }

}
