package io.takima.demo.Service.appRole;

import io.takima.demo.Repository.AppRoleRepository;
import io.takima.demo.Role.AppRole;
import io.takima.demo.Role.UserRole;
import io.takima.demo.Service.appRole.AppRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class AppRoleServiceImpl implements AppRoleService {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private AppRoleRepository appRoleRepository;

    public List<String> getRoleNames(Long userId) {
        String sql = "Select ur.appRole.roleName from " + UserRole.class.getName() + " ur " //
                + "Where ur.appUser.userId = :userId";

        Query query = this.entityManager.createQuery(sql, String.class);
        query.setParameter("userId", userId);

        return query.getResultList();
    }


    @Override
    public AppRole findByRoleName(String roleName) {
        return appRoleRepository.findByRoleName(roleName);
    }
}
