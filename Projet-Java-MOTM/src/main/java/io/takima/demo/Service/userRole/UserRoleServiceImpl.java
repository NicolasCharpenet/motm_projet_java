package io.takima.demo.Service.userRole;

import io.takima.demo.Repository.UserRoleRepository;
import io.takima.demo.Role.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Override
    public UserRole enregistrerUserRole(UserRole userRole) {
        return userRoleRepository.save(userRole);
    }
}
