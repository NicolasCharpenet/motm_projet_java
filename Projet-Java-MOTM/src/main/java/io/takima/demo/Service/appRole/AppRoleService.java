package io.takima.demo.Service.appRole;

import io.takima.demo.Role.AppRole;

public interface AppRoleService {

    public AppRole findByRoleName(String roleName);
}
