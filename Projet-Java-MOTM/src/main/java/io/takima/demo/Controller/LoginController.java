package io.takima.demo.Controller;

import io.takima.demo.Repository.AppUserRepository;
import io.takima.demo.Role.AppUser;
import io.takima.demo.security.LoginViewModel;
import io.takima.demo.security.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class LoginController {

    @Autowired
    private AppUserRepository appUserRepository;

    // la page de login sera retournée à l'utilisateur s'il souhaite se déconnecter

    RestTemplate restTemplate = new RestTemplate();
    @GetMapping(value = {"/login"})
    public String loginPage(Model model) {
        model.addAttribute("loginViewModel",new LoginViewModel());
        return "loginPage";
    }

    // quand l'utilisateur va saisir cette url, la page loginPage lui sera retournée pour l'authentification

    @GetMapping(value = {"/logout"})
    public String logoutPage(Model model) {
        return "login";
    }

    @GetMapping(value = {"/index"})
    public String index(Model model, Model m, @AuthenticationPrincipal UserDetails currentUser) {
        AppUser appUser = appUserRepository.findByUserName(currentUser.getUsername());
        model.addAttribute("connexion",appUser);

        return "index";
    }


    @GetMapping("/success")
    public String redirectPage(Model model, @CookieValue String username, @CookieValue String role) {
        if(username == null){
            return "redirect:/login";
        }

        model.addAttribute("username",username);
        model.addAttribute("role",role);
        return "successPage";
    }

}
