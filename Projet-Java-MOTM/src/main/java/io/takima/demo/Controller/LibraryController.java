package io.takima.demo.Controller;

import io.takima.demo.Modele.User;
import io.takima.demo.UserDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 *
 */
//@RequestMapping("/")
@Controller
public class LibraryController {



    private final UserDAO userDAO;

    public LibraryController(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

@GetMapping("/motm_survey/{id}")
public String motmSurvey(Model m, @PathVariable(name= "id") Long id) {
    Optional<User> user = userDAO.findById(id);
    System.out.println(user.toString());
    m.addAttribute("user", user.get());
    return "motm_survey";
}

    @PostMapping("/motm_survey/{id}")
    public RedirectView motmSurvey(@ModelAttribute User user, RedirectAttributes attrs, @PathVariable(name= "id") Long id) {
        User dbUser = userDAO.findById(id).orElseThrow(() -> new NoSuchElementException("No user with this id"));
        dbUser.setFirstName(user.getFirstName());
        dbUser.setLastName(user.getLastName());
        dbUser.setAge(user.getAge());
        dbUser.setDatenaiss(user.getDatenaiss());
        dbUser.setEmail(user.getEmail());
        dbUser.setMotm(user.getMotm());
        userDAO.save(dbUser);


        return new RedirectView("/index1");
    }
    @GetMapping("/index1")
    public String homePage(Model m) {
        m.addAttribute("users", userDAO.findAll());

        return "index1";
    }

}
