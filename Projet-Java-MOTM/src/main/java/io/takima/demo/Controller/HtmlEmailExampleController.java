package io.takima.demo.Controller;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import io.takima.demo.Modele.User;
import io.takima.demo.Service.user.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/admin")
@Controller
public class HtmlEmailExampleController {

    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    public UserServiceImpl userServiceImpl;

    @ResponseBody
    @RequestMapping(value = "/sendHtmlEmail/{email}", method = RequestMethod.GET)
    public String sendHtmlEmail(@PathVariable(name = "email" ) String email) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();

        User user = userServiceImpl.findByEmail(email);
        long id = user.getId();
        boolean multipart = true;

        MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");

        String htmlMsg = "<a href='http://localhost/motm_survey/"+id+"'>Veuillez remplir le formulaire suivant</a>"
                +"<img src='http://www.apache.org/images/asf_logo_wide.gif'>";

        message.setContent(htmlMsg, "text/html");

        helper.setTo(email);


        helper.setSubject("Lien pour remplir le formulaire");


        this.emailSender.send(message);

        return "Email Sent!";
    }

}
