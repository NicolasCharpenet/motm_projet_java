package io.takima.demo.Controller;


import io.takima.demo.Modele.User;
import io.takima.demo.Role.AppUser;
import io.takima.demo.Service.appRole.AppRoleServiceImpl;
import io.takima.demo.Service.appUser.AppUserServiceImpl;
import io.takima.demo.Service.user.UserServiceImpl;
import io.takima.demo.Service.userRole.UserRoleServiceImpl;
import io.takima.demo.UserDAO;
import io.takima.demo.Utils.Password;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.NoSuchElementException;
import java.util.Optional;

@RequestMapping("/admin")
@Controller
public class AdminController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private AppUserServiceImpl appUserService;

    @Autowired
    private AppRoleServiceImpl appRoleService;

    @Autowired
    private UserRoleServiceImpl userRoleService;

    @Autowired
    public JavaMailSender emailSender;

    private final UserDAO userDAO;

    public AdminController(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @GetMapping
    public String homePage(Model m) {
        m.addAttribute("users", userDAO.findAll());

        return "redirect:/loginPage";
    }

    @GetMapping("/new")
    public String addUserPage(Model m) {
        m.addAttribute("user", new User());
        return "new";
    }

    @PostMapping("/new")
    public RedirectView createNewUser(@ModelAttribute User user, RedirectAttributes attrs) throws MessagingException {

        if (userService.findByEmail(user.getEmail()) != null ) {
            return new RedirectView ("new");
        }

        attrs.addFlashAttribute("message", "Utilisateur ajouté avec succès");
        user.setMotm(0);
        userDAO.save(user);


        // lorsque l'utilisateur est crée, son password est généré
        String password = new Password().generate(user, "USER");
        String mailUser = user.getEmail();

        //le password de l'utilisateur lui est envoyé par mail
        envoyerLeMotDePasseParDefaut( mailUser,password);
        System.out.println(password);

        AppUser user1 = new AppUser(user.getEmail(), passwordEncoder.encode(password),  true, "USER", user);
        user1 = appUserService.enregistrerAppuser(user1);

        return new RedirectView("/admin/dashboard");
    }

    @GetMapping("/dashboard")
    public String dashboard(Model m) {
        m.addAttribute("users", userDAO.findAll());
        return "dashboard";
    }


    @Secured(value = {"ROLE_admin"})
    @GetMapping("/editemployee/{id}")
    public String editUser(Model m, @PathVariable(name= "id") Long id) {
        Optional<User> user = userDAO.findById(id);
        System.out.println(user.toString());
        m.addAttribute("user", user.get());
        return "editUser";
    }

    @PostMapping("/editemployee/{id}")
    public RedirectView editNewUser(@ModelAttribute User user, RedirectAttributes attrs, @PathVariable(name= "id") Long id) {
        User dbUser = userDAO.findById(id).orElseThrow(() -> new NoSuchElementException("No user with this id"));
        dbUser.setAge(user.getAge());
        dbUser.setDatenaiss(user.getDatenaiss());
        dbUser.setEmail(user.getEmail());
        userDAO.save(dbUser);

        attrs.addFlashAttribute("message", "Utilisateur modifié avec succès");
        return new RedirectView("/admin/dashboard");
    }


    @Secured(value = {"ROLE_admin"})
    @GetMapping("/delete/{id}")
    public String deleteEmployee (Model model, @PathVariable(name = "id")Long id) {
        System.out.println(id);
        userDAO.deleteById(id);
        return "redirect:/admin/dashboard";
    }



    // méthode qui permet d'envoyer à l'utilisateur crée, son password crypté par mail. Il pourra ainsi le récupérer et se connecter

    public void envoyerLeMotDePasseParDefaut(String email, String password) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();

        boolean multipart = true;

        MimeMessageHelper helper = new MimeMessageHelper(message,multipart,"utf-8");

        String htmlMsg = "Bonjour, <br> Votre mot de passe par défaut est "+ password;

        message.setContent(htmlMsg,"text/html");

        helper.setTo(email);

        helper.setSubject("Mot de passe par défaut");

        this.emailSender.send(message);

    }

}
